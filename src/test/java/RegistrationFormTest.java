import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.RegistrationFormPage;

public class RegistrationFormTest extends TestBase {
    RegistrationFormPage registrationPage;

    @BeforeMethod
    public void refgistrationTestBase() {
        registrationPage = new RegistrationFormPage(driver);
    }

    @Test
    public void formFillTest() {
        registrationPage.setName("Jan", "Nowak")
            .setSex()
            .setExperience()
            .setDate("25.01.1991")
            .setProfession()
            .uploadPhoto("C:\\Users\\Katarzyna\\Pictures\\MAGICC_logo_small.jpg")
            .setTool()
            .setContinent("Europe")
            .setSeleniumComand("Wait Commands")
            .clickSubmitButton();
    }

    @Test
    public void formElementsDisplayTest() {
        Assert.assertTrue(registrationPage.getFormHeader().isDisplayed(),"Form header didn't display");
        Assert.assertTrue(registrationPage.getPartialLinkTest().isDisplayed(),"Partial Link Test didn't display");
        Assert.assertTrue(registrationPage.getLinkTest().isDisplayed(),"Link Test didn't display");
        Assert.assertTrue(registrationPage.getDownloadSeleniumLink().isDisplayed(),"Link to download Selenium didn't display");
        Assert.assertTrue(registrationPage.getDownloadTestLink().isDisplayed(),"Link to download Test File didn't display");
    }

}
