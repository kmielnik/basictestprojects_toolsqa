package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import java.util.List;
import java.util.Random;

public class RegistrationFormPage extends PageObject {

    @FindBy (xpath = "//div[@class='wpb_wrapper']//h1")
    private WebElement formHeader;

    @FindBy (xpath = "//a[@title='Automation Practice Form']")
    private WebElement partialLinkTest;

    @FindBy (xpath = "//a[@title='Automation Practice Table']")
    private WebElement linkTest;

    @FindBy(name="firstname")
    private WebElement firstName;

    @FindBy (name="lastname")
    private WebElement lastName;

    @FindBy (xpath = "//input[@name='sex']")
    private List<WebElement> sexList;

    @FindBy (xpath = "//input[@name='exp']")
    private List<WebElement> experienceList;

    @FindBy (id="datepicker")
    private WebElement date;

    @FindBy (xpath = "//input[@name='profession']")
    private List<WebElement> professionList;

    @FindBy (id = "photo")
    private WebElement photo;

    @FindBy (xpath = "//a[@href='http://toolsqa.wpengine.com/wp-content/uploads/2014/04/OnlineStore.zip']")
    private WebElement downloadSeleniumLink;

    @FindBy (xpath = "//a[@href='http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/uploads/2016/09/Test-File-to-Download.xlsx']")
    private WebElement downloadTestLink;

    @FindBy (xpath = "//input[@name='tool']")
    private List<WebElement> toolList;

    @FindBy (id = "continents")
    private WebElement continents;
    Select continent = new Select(continents);

    @FindBy (id = "selenium_commands")
    private WebElement seleniumCommands;
    Select seleniumCommand = new Select(seleniumCommands);

    @FindBy (id = "submit")
    private WebElement submitButton;

    public RegistrationFormPage(WebDriver driver) {
        super(driver);
    }

    public RegistrationFormPage setName(String firstName, String lastName) {
        this.firstName.sendKeys(firstName);
        this.lastName.sendKeys(lastName);
        return this;
    }

    public RegistrationFormPage setSex() {
        getRandomElement(this.sexList).click();
        return this;
    }

    public RegistrationFormPage setExperience() {
        getRandomElement(this.experienceList).click();
        return this;
    }

    public RegistrationFormPage setDate(String date) {
        this.date.sendKeys(date);
        return this;
    }

    public RegistrationFormPage setProfession() {
        getRandomElement(this.professionList).click();
        return this;
    }

    public RegistrationFormPage uploadPhoto(String photoUrl) {
        this.photo.sendKeys(photoUrl);
        return this;
    }

    public RegistrationFormPage setTool() {
        getRandomElement(this.toolList).click();
        return this;
    }

    public RegistrationFormPage clickSubmitButton() {
        this.submitButton.click();
        return this;
    }

    public RegistrationFormPage setContinent(String answer) {
        this.continent.selectByVisibleText(answer);
        return this;
    }

    public RegistrationFormPage setSeleniumComand(String comand) {
        this.seleniumCommand.selectByVisibleText(comand);
        return this;
    }

    public WebElement getFormHeader() {
        return formHeader;
    }

    public WebElement getPartialLinkTest() {
        return partialLinkTest;
    }

    public WebElement getLinkTest() {
        return linkTest;
    }

    public WebElement getDownloadSeleniumLink() {
        return downloadSeleniumLink;
    }

    public WebElement getDownloadTestLink() {
        return downloadTestLink;
    }
}
